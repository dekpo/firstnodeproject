// import de mongoose
const mongoose = require('mongoose');
// import d'express
const express = require('express');
// on lance le serveur express sur le port 3000
const appWeb = express();
appWeb.listen(3000);
// on définit la route de la racine si besoin...
appWeb.get('/',(request,response) => {
    response.send("THE SERVER IS ALIVE !!!");
    console.log(request.query);
});

const URI = 'mongodb+srv://dekpo:qi08xn6@cluster0.i87hs.mongodb.net/sample_airbnb?retryWrites=true&w=majority';
// on se connecte au serveur
mongoose.connect(URI,{ useUnifiedTopology: true, useNewUrlParser: true },(error) => {
    if (error) console.log('Error',error);
    console.log("Connected to MongoDB !!!");
});
// on définit un model basé sur un schéma en fonction de la collection
const mySchema = new mongoose.Schema({});
const myModel = mongoose.model('airbnb',mySchema,'listingsAndReviews');
// on crée une requête
const count = myModel.find().countDocuments();
// on execute la requête
count.exec((err,result) => {
    console.log('Resultat du count', result);
});

// sur la route /api du serveur express on va pouvoir retourner les résultats de notre BDD MongoDB
appWeb.get('/api',(request,response) => {

    const limit = request.query.limit ? parseInt( request.query.limit ) : 10;
    const country = request.query.country ? request.query.country : 'Canada';

    const listing = myModel.find( {'address.country':country},{name:1,listing_url:1,address:1} ).limit( limit );

    listing.exec((err,result) => {
        response.send( result );
    });

});
